<?php


namespace App;


use App\Algorithm\AlgorithmInterface;

class Password implements PasswordInterface
{
    protected string $password;

    /**
     * Password constructor.
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function hash(AlgorithmInterface $algorithm): string
    {
        return password_hash($this->password, $algorithm->getIdentifier(), $algorithm->getOptions());
    }

    /**
     * @inheritDoc
     */
    public function verify(string $hash): bool
    {
        return password_verify($this->password, $hash);
    }

    /**
     * @inheritDoc
     */
    public static function needsRehash($hash, AlgorithmInterface $algorithm): bool
    {
        return password_needs_rehash($hash, $algorithm->getIdentifier(), $algorithm->getOptions());
    }
}