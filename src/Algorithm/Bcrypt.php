<?php

namespace App\Algorithm;

class Bcrypt implements AlgorithmInterface
{
    private $identifier = PASSWORD_BCRYPT;
    private ?string $salt;
    private ?int $cost;

    /**
     * Bcrypt constructor.
     * @param string $salt
     * @param int $cost
     */
    public function __construct(?string $salt = null, ?int $cost = null)
    {
        $this->salt = $salt;
        $this->cost = $cost;
    }

    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @inheritDoc
     */
    public function getOptions(): array
    {
        $options = [
            'salt' => $this->salt,
            'cost' => $this->cost,
        ];

        //Delete empty options and return
        return array_diff($options, [ null, 0, '']);
    }
}