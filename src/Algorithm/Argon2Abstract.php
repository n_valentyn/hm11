<?php

namespace App\Algorithm;

abstract class Argon2Abstract implements AlgorithmInterface
{
    protected $identifier;
    protected int $memory_cost;
    protected int $time_cost;
    protected int $threads;

    /**
     * Argon2Abstract constructor.
     * @param int $memory_cost
     * @param int $time_cost
     * @param int $threads
     */
    public function __construct(int $memory_cost = PASSWORD_ARGON2_DEFAULT_MEMORY_COST,
                                int $time_cost = PASSWORD_ARGON2_DEFAULT_TIME_COST,
                                int $threads = PASSWORD_ARGON2_DEFAULT_THREADS)
    {
        $this->memory_cost = $memory_cost;
        $this->time_cost = $time_cost;
        $this->threads = $threads;
    }

    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @inheritDoc
     */
    public function getOptions(): array
    {
        return [
            'memory_cost' => $this->memory_cost,
            'time_cost' => $this->time_cost,
            'threads' => $this->threads,
        ];
    }

}