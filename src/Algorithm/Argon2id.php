<?php

namespace App\Algorithm;

use App\Algorithm\Argon2Abstract;

class Argon2id extends Argon2Abstract
{
    protected $identifier = PASSWORD_ARGON2ID;
}