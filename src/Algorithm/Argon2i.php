<?php

namespace App\Algorithm;

use App\Algorithm\Argon2Abstract;

class Argon2i extends Argon2Abstract
{
    protected $identifier = PASSWORD_ARGON2I;
}